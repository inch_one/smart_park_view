import Vuex from 'vuex';
import Vue from 'vue';
import {baseURL} from '../util/axiosPlugin.js'
Vue.use(Vuex);
let store = new Vuex.Store({
	state: {
		baseURL:baseURL,
		currentPark:{
			id:"",
			name:"",
		},
		roles:[],
		userInfo:null,
		// userInfo:{
		// 	nickname: null,
		// 	realName: null,
		// 	phone: null,
		// 	email: null,
		// 	avatarUrl: null,
		// 	age: null,
		// 	gender: null,
		// 	enterpriseId: null,
		// 	parkId: null,
		// 	statu: null,
		// 	roles:null,
		// },
		tabbarList: [{
				iconPath: "home",
				selectedIconPath: "home-fill",
				text: '首页',
				customIcon: false,
				pagePath: "/pages/core/index/index"
			},
			{
				iconPath: "grid",
				selectedIconPath: "grid-fill",
				text: '服务',
				customIcon: false,
				pagePath: "/pages/core/service/service"
			},

			{
				iconPath: "bell",
				selectedIconPath: "bell-fill",
				// count: 23,
				text: '协作',
				customIcon: false,
				pagePath: "/pages/core/chat/chat"
			},
			{
				iconPath: "account",
				selectedIconPath: "account-fill",
				text: '我的',
				customIcon: false,
				pagePath: "/pages/core/mine/mine"
			},
		],
		tabbarConfig: {
			activeColor: "#007aff",
			inactiveColor: "#535353",
			borderTop:false,
		}
	},
	actions: {

	},
});

export default store;
