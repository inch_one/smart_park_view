// vue.config.js
let path = require('path');
let stylePath = path.resolve(__dirname, 'custom.less')

const TransformPages = require('uni-read-pages')
const {webpack} = new TransformPages()
module.exports = {
	css: {
		loaderOptions: {
			less: {
				globalVars: {
					"hack": `true; @import "${stylePath}"`
				}
			}
		}
	},
	configureWebpack: {
		plugins: [
			new webpack.DefinePlugin({
				ROUTES: webpack.DefinePlugin.runtimeValue(() => {
					const tfPages = new TransformPages({
						includes: ['path', 'name', 'aliasPath','meta']
					});
					return JSON.stringify(tfPages.routes)
				}, true )
			}),
		
		],
		
	}

}
