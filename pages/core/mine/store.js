import Vuex from 'vuex';

let store = new Vuex.Store({
	state: {
		swiperList: [{
				image: '../../../static/temp/swiper_test1.png',
			},
			{
				image: '../../../static/temp/swiper_test2.png',
			},
		],
		tabsList: [{
			name: '园区公告',
			count: 5,
			articleList:[{
				title:"新一轮惠企政策不断加码 企业发展'马力足'",
				summary:"中小微企业在经济社会发展中具有独特的作用,是国民经济和社会发展的重要基础,在扩大就业、增加收入、改善民生、国家",
				isTop:true,
				readNum:168,
				pubdate:"1个月前",
				imgList:["http://a2.img.fengone.com/9719e2fda104631cfdd3e2776f99ed98@100Q_680w.png","http://a2.img.fengone.com/9719e2fda104631cfdd3e2776f99ed98@100Q_680w.png"]
			},{
				title:"新一轮惠企政策不断加码 企业发展'马力足'",
				summary:"中小微企业在经济社会发展中具有独特的作用,是国民经济和社会发展的重要基础,在扩大就业、增加收入、改善民生、国家",
				isTop:true,
				readNum:155,
				pubdate:"1个月前",
				imgList:[]
			},{
				title:"新一轮惠企政策不断加码 企业发展'马力足'",
				summary:"中小微企业在经济社会发展中具有独特的作用,是国民经济和社会发展的重要基础,在扩大就业、增加收入、改善民生、国家",
				isTop:false,
				readNum:190,
				pubdate:"2个月前",
				imgList:["http://a2.img.fengone.com/9719e2fda104631cfdd3e2776f99ed98@100Q_680w.png"]
			}]
		}, {
			name: '企业政策',
			articleList:[],
		},{
			name: '园区服务',
			articleList:[],
		}, {
			name: '园区活动',
			articleList:[],
		}],
		tabsCurrent:0,
		tabsSwiperCurrent:0,
	},
	actions: {

	},
});
export default store;
