import Vue from 'vue'
import popupComponent from './popup.vue'

export default{
	install(Vue,options){
		Vue.$popup=Vue.prototype.$popup=function(options){
			console.log("testMounted")
			const PopupConstructor=Vue.extend(popupComponent)
			const div=document.createElement('div')
			document.body.appendChild(div)
			const vm=new PopupConstructor({
				propsData:options,
			}).$mount(div)
		}
	}
}
// export function popup(){
// 	console.log("testMounted")
// 	const PopupConstructor=Vue.extend(popupComponent)
// 	const div=document.createElement('div')
// 	document.body.appendChild(div)
// 	const vm=new PopupConstructor({

// 	}).$mount(div)
// }