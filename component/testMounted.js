import Vue from 'vue'
import popupComponent from './popup.vue'

const Login = {
  install (Vue, options) {
    Vue.prototype.$loginPopup = function (options) {
		console.log("testMountedLogin")

      const LoginConstructor = Vue.extend(popupComponent)
      const div = document.createElement('div')
      document.body.appendChild(div)
      const vm = new LoginConstructor({
        propsData: options,
      }).$mount(div)

    }
  }
}
 
export default Login
