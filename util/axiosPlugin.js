import axios from 'axios'
import axiosAdapterUniapp from 'axios-adapter-uniapp'
import {router,RouterMount} from '../router/router.js'  

//export const baseURL='http://mota.oriole.cn/';
// export const baseURL='http://139.9.104.172:8090';
export const baseURL='http://localhost:8090/';
import adapter from "./adapter.js"


export const Axios = axios.create({
    baseURL: baseURL,
    timeout: 5*60000,
	adapter: axiosAdapterUniapp
});
// Axios.defaults.adapter = adapter;
// Axios.defaults.retry = 5; // 设置请求次数

Axios.defaults.retryDelay = 1000;// 重新请求时间间隔
//请求拦截器
Axios.interceptors.request.use(config => {
	console.log("url",config.url)
 //    console.log("请求头",uni.getStorageSync("token"))
	
    //若存在这样的token，则从本地存储取出
    if (uni.getStorageSync("token")) {
        config.headers.Authorization = uni.getStorageSync("token")
    }
    return config
}, error => {
    return Promise.reject(error)
});

//响应拦截器
Axios.interceptors.response.use(res => {

    return res
}, error => {
    console.log("erroe",error);
    if (error.response.status === 401||error.response.status === 403) {
        // 401表明用户登录状态已经丢失，需跳转至登录页面重新登录获取 token
        router.push("/pages/login/login");		
    }
    return Promise.reject(error.response.data)
});

export default {
    install(Vue) {
        Object.defineProperty(Vue.prototype, '$Axios', {value: Axios})
    }
}