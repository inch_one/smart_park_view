import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
import Vuex from 'vuex';
import store from 'store/store.js';
import axiosPlugin,{baseURL} from './util/axiosPlugin.js'

import navBar from 'component/navBar.vue'
import Popup from 'component/popup.js'
import Login from "component/testMounted.js"
import {router,RouterMount} from './router/router.js'  
Vue.use(router)
Vue.use(Vuex);
Vue.use(axiosPlugin);
Vue.use(uView);
Vue.use(Popup)

Vue.use(Login)
Vue.config.productionTip = false

App.mpType = 'app'


  // 全局注册组件
  Vue.component(
	'v-nav-bar',navBar
)

const app = new Vue({
    ...App,
	store,
	data(){
		return {
			app:"app"
		}
	}
	
})
app.$mount()
